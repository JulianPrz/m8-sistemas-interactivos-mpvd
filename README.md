# M8 Sistemas Interactivos MPVD

Módulo 8 Sistemas Interactivos en la red para el Máster de Periodismo de Datos en la Universidad de Alcalá. https://mpvd.es/modulo/08-sistemas-interactivos/

## Slides

- [S0. Introducción módulo](https://julianprz.gitlab.io/m8-sistemas-interactivos-mpvd/main/sesiones/S0/M8S0.html)
- [S1. Intro Web](https://julianprz.gitlab.io/m8-sistemas-interactivos-mpvd/main/sesiones/S1/M8S1.html)
